#!/bin/sh

if [ $(cat modinfo.json | jq -r .type) = 'code' ]
then
  echo "Building code:"
  dotnet build
else
  echo "Mod is content only, skipping code build."
  exit 0
fi
