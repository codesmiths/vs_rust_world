#!/bin/sh

# Do this first:
# sudo apt install pngquant

# Optimize all PNG textures by turning them into palette files
find assets/rustworld/textures assets/game/textures -type f -path "*raw*" -name "*.png" | xargs -L1 pngquant -f --ext .png --quality 90-95 -s 3
