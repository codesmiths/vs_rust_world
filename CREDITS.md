This mod would have not been possible with the help of countless other people. We stand on the shoulders of giants!

Thank you,

~Tels & Phiwa

# Contributors

Without you, this mod would not have been possible - thank you!

## Thank you

First and foremost a big thank you goes to **Saraty**, **Tyron** and all the other
team members for creating such a great game!

So many people tested the mod, reported bugs and requested features, here
is a hearty **Thank you!**!

### Testing, Feedback and Bugreporting

*

### Translators

* German: **Tels**, **Phiwa**

### Modding help

* All the people answering (our and others) questions, on the modding-help discord channel or elsewere: You are awesome!
Especially:
  + **l33tmaan**

