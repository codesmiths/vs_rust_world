stages:
    - pre_build_test
    - build
    - post_build_test
    - release

pre_build_test:
    image: debian:bullseye-slim
    stage: pre_build_test
    tags:
        - runner_vps
    rules:
        - if: '$CI_PIPELINE_SOURCE == "push"'
          when: always
        - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
          when: always
    before_script:
        - echo "Preparing pre_build_test"
        # Install prerequisities
        - apt-get update && apt-get upgrade -y
        - apt-get install --no-install-recommends -y file libarchive-zip-perl libjson-perl libipc-run-perl libjson-validator-perl
        # Install packacges used by get_dependencies.pl
        - apt-get install -y --no-install-recommends libsemver-perl libwww-curl-perl
    script:
        - echo "Running pre_build_test"
        # Run the prebuild tests
        - prove test/prebuild/*.t
    after_script:
        - echo "Post-processing pre_build_test"

build:
    image: debian:bullseye-slim
    stage: build
    tags:
        - runner_vps
    rules:
        - if: '$CI_PIPELINE_SOURCE == "push"'
          when: always
        - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
          when: always
    artifacts:
        when: on_success
        expire_in: 5 days
        paths:
            - dist/
    before_script:
        - apt-get update && apt-get upgrade -y
        # Install prerequisities
        - apt-get install --no-install-recommends -y apt-transport-https ca-certificates wget file libarchive-zip-perl libjson-perl libipc-run-perl libsemver-perl libwww-curl-perl jq
        # Download and install the package with the MS package list and GPG key
        - wget --no-verbose https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O /tmp/pkg-ms-prod.deb
        - dpkg -i /tmp/pkg-ms-prod.deb
        # update the repository with the MS packages
        - apt-get update && apt-get upgrade -y
        # Disable telemetry
        - export DOTNET_CLI_TELEMETRY_OPTOUT=1
        # Install dotnet
        - apt-get install --no-install-recommends -y dotnet-sdk-5.0
        # Extract minimum required game/server version from modinfo.json
        - export SERVER_VERSION=$(cat modinfo.json | jq -r .dependencies.game)
        - export SERVER_STABLE=$(cat modinfo.json | jq -r .dependencies.game | perl -wne '$a = "stable"; $a = $1 if $_ =~ /(pre|rc)/; print $a')
        # Download VS
        - mkdir -p /home/vintagestory/server
        - mkdir -p /home/vintagestory/server/Mods/
        - mkdir -p /home/vintagestory/.config/VintagestoryData
        - cd /home/vintagestory/server
        # Download specified game version
        - wget --no-verbose http://bloodgate.com/mirrors/vs/${SERVER_STABLE}/vs_server_${SERVER_VERSION}.tar.gz || wget --no-verbose https://cdn.vintagestory.at/gamefiles/${SERVER_STABLE}/vs_server_${SERVER_VERSION}.tar.gz
        # Extract server
        - tar -xzf vs_server_*.tar.gz && rm vs_server_*.tar.gz
        # cd back
        - cd /builds/codesmiths/vs_rust_world/
        # Download mod dependencies (we might need them for the build?)
        - perl ${CI_PROJECT_DIR}/build/get_dependencies.pl /home/vintagestory/server/Mods/
        # Export the proper environment variables for the build stage
        - export VINTAGE_STORY=/home/vintagestory/server
        - export VINTAGE_STORY_DATA=/home/vintagestory/server/.config/VintagestoryData
    script:
        # Compile the DLL if we have a `code` mod
        - /bin/sh build/build_code.sh
        # Run package script
        - perl build/bundle_mod.pl
    after_script:
        # Move bundled mod to dist/ directory, so it is uploaded as an artifact
        - mkdir dist/
        - mv rustworld-*.zip dist/

post_build_test:
    image: debian:bullseye-slim
    stage: post_build_test
    tags:
        - runner_vps
    rules:
        - if: '$CI_PIPELINE_SOURCE == "push"'
          when: always
        - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
          when: always
    before_script:
        - echo "Preparing post_build_test"
        # Install prerequisities
        - apt-get update && apt-get upgrade -y
        - apt-get install --no-install-recommends -y file libarchive-zip-perl libjson-perl libipc-run-perl
    script:
        - echo "Running post_build_test"
        # Run the postbuild tests
        - prove test/postbuild/*.t
    after_script:
        - echo "Post-processing post_build_test"

start_server:
    image: debian:bullseye-slim
    stage: post_build_test
    tags:
        - runner_vps
    # Only run this time-consuming job for merge requests or main branch
    rules:
        - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
          when: always
        - if: '$CI_COMMIT_BRANCH == "main"'
          when: always
        - when: never
    # Download artifacts of job "build"
    dependencies:
        - build
    before_script:
        # Install VintageStory server, following HowTo found at
        #   https://wiki.vintagestory.at/index.php?title=Setting_up_a_Multiplayer_Server#Requirements_for_Ubuntu_.28Debian.2C_Mint.2C_....29
        # Install required packages
        - apt-get update && apt-get upgrade -y
        - apt-get install -y --no-install-recommends screen wget curl ack jq
        # Install packages used by build/get_dependencies.pl
        - apt-get install -y --no-install-recommends libjson-perl libsemver-perl libwww-curl-perl
        # Install Mono
        - apt-get install -y apt-transport-https dirmngr gnupg ca-certificates
        - apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
        # We need to use the buster repository, as there is no mono bullseye repository yet
        - echo "deb https://download.mono-project.com/repo/debian stable-buster main" | tee /etc/apt/sources.list.d/mono-official-stable.list
        # Install mono and then try to free up some space
        - apt-get update && apt-get install -y --no-install-recommends mono-complete && apt autoremove
        # list the mono version for confirmation
        - mono --version
        # Extract minimum required game/server version from modinfo.json
        - export SERVER_VERSION=$(cat modinfo.json | jq -r .dependencies.game)
        - export SERVER_STABLE=$(cat modinfo.json | jq -r .dependencies.game | perl -wne '$a = "stable"; $a = $1 if $_ =~ /(pre|rc)/; print $a')
        # Create user and server directory (as expected by vintagestory server by default)
        - adduser --quiet --disabled-password vintagestory
        - mkdir -p /home/vintagestory/server
        - mkdir -p /home/vintagestory/server/Mods
        - mkdir -p /var/vintagestory/data/Logs/Logs
        - cd /home/vintagestory/server
        # Download specified game version
        - wget --no-verbose http://bloodgate.com/mirrors/vs/${SERVER_STABLE}/vs_server_${SERVER_VERSION}.tar.gz || wget --no-verbose https://cdn.vintagestory.at/gamefiles/${SERVER_STABLE}/vs_server_${SERVER_VERSION}.tar.gz
        # Extract server package
        - tar -xzf vs_server_*.tar.gz && rm vs_server_*.tar.gz
        # Make server executable
        - chmod +x server.sh
        # Copy mod to server's mod directory
        - cp ${CI_PROJECT_DIR}/dist/rustworld-*.zip /home/vintagestory/server/Mods/
        # Download mod dependencies
        - perl ${CI_PROJECT_DIR}/build/get_dependencies.pl /home/vintagestory/server/Mods/
        # Show the result
        - ls -la /home/vintagestory/server/Mods/
        - ls -la /home/vintagestory/
    script:
        # Run server and wait a bit
        - ./server.sh start || cat /var/vintagestory/data/Logs/server-main.txt
        - sleep 2
        # check the server status
        - echo "Checking server status." && ./server.sh status
        # Wait for server to start and afterwards evaluate the server log
        # for specific messages to verify the mod could be loaded successfully
        - bash ${CI_PROJECT_DIR}/test/server/test_server_loading_mod.sh
    after_script:
        - echo "Cleanup"

sign_release:
    image: debian:bullseye-slim
    stage: release
    tags:
        - runner_vps
    # Only create a signed release 
    rules:
        - if: '$CI_COMMIT_BRANCH == "main"'
          when: always
    # Download artifacts of job "build"
    dependencies:
        - build
    artifacts:
        when: on_success
        # Currently, this is meant to store all release indefinitely.
        # If there is another stage added, this could be reduced.
        expire_in: 3 months
        paths:
            - dist/
    before_script:
        # Install required packages
        - apt-get update && apt-get upgrade -y
        - apt-get install -y --no-install-recommends gpg gpg-agent expect
        # Write filename to sign to ENV_VARIABLE
        - RELEASE_FILE=`find ${CI_PROJECT_DIR}/dist/ -iname 'rustworld-*.zip'`
        # Load public/secret keys from pipeline secrets
        - gpg --import ${SIGNATURE_KEY_GPG_PUBLIC_FILE}
        - gpg --import ${SIGNATURE_KEY_GPG_SECRET_FILE}
        # Get key identifier to use for signing
        - GPG_KEY_ID=`gpg --list-secret-keys | egrep -o '[0-9A-F]{40}'`
        # Set the trust level of the key to full trust
        - expect -c "spawn gpg --edit-key ${GPG_KEY_ID} trust quit; send \"5\ry\r\"; expect eof"
    script:
        # Create a detached signature with GnuPG
        - gpg --sign --quiet --armor --local-user ${GPG_KEY_ID} --detach-sig --output ${RELEASE_FILE}.asc ${RELEASE_FILE}
        # Verify the detached signature with GnuPG (will return code 1 if signature is invalid)
        - gpg --verify ${RELEASE_FILE}.asc ${RELEASE_FILE}
    after_script:
        - echo "Cleanup"
