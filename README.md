# Rust World

"When sanity leaves, the rust sneaks in."

This Vintage Story mod adds new, interesting and dangerous things
in deep caves and areas where the temporal stability is low.

## More Information

You can find more information and pretty pictures at our [Wiki](https://gitlab.com/codesmiths/vs_rust_world/-/wikis/home).

## Support Our Work

If you want to support us, please become a [Patron](https://www.patreon.com/telsandphiwa).

We hope you enjoy our work.

~Phiwa & Tels

# Download

* The mod from [Official Vintage Story ModDB](https://mods.vintagestory.at/rustworld)
* The source code can be found on [Gitlab](https://gitlab.com/codesmiths/vs_rustworld/-/releases)

# Installation

This mod should work in existing worlds. If in doubt, please create a new world.

  <u>**Make a backup of your savegame and world before trying this mod!**</u>

# Features

## TOBEDONE

*

# Languages

* English (100%)
* German (100%)

If you would like to translate this mod into other languages, please [contact us](https://gitlab.com/codesmiths/vs_bricklayers/-/wikis/Contact).

# Known issues

None yet.

# Changes

Please see the [Changelog](Changelog) for the changes in the latest release.

If you have any ideas, or know about features that already done in another mod, then we
love [to hear from you](https://gitlab.com/codesmiths/vs_bricklayers/-/wikis/Contact).

# Signature key

All our releases are signed using PGP (GnuPG) and their integrity can be verified by using the public key as published
[on the wiki](https://gitlab.com/codesmiths/vs_bricklayers/-/wikis/Signing-key).

