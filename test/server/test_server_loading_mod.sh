#!/bin/bash

# This script evaluates the server log and checks for problems while loading the mod.

# Example log entries this script is searching for:
#
#     21.1.2021 22:55:00 [Notification] Found 4 mods (0 disabled)
#     21.1.2021 22:55:00 [Notification] Mods, sorted by dependency: game, creative, survival, bricklayers
#     21.1.2021 22:55:09 [Warning] Failed resolving a blocks itemdrop or smeltedstack with code bricklayers:resin in cooking recipe bricklayers:recipes/cooking/glueportion.json
#     31.1.2021 22:55:20 [Event] Dedicated Server now running on Port 42420 and all ips!

MODNAME='rustworld'

PREFIX='^[0-9\.\:_ -]+'

LOGFILE='/var/vintagestory/data/Logs/server-main.txt'

# Wait for server to start
runtime="5 minute"
endtime=$(date -ud "$runtime" +%s)

server_startup_successful=false

while [[ $(date -u +%s) -le $endtime ]]
do
    # Check if server is up
    egrep "$PREFIX\[Event\] Dedicated Server now running on Port [0-9]+ and all ips" $LOGFILE

    if [ $? -eq 0 ]; then
        echo "Server startup finished"
        server_startup_successful=true
        break
    else
        echo "Server startup not finished yet, waiting for five seconds"
        sleep 5
    fi
done

if [ "$server_startup_successful" = false ]; then
    echo "Server did not start up within $runtime"
    exit 1
fi


# Line in log: 21.1.2021 22:55:00 [Notification] Found 4 mods (0 disabled)

# Check if all mods were loaded successfully
echo "Checking if all mods were loaded..."
egrep "$PREFIX\[Notification\] Found [0-9] mods \(0 disabled\)" $LOGFILE

if [ $? -eq 0 ]; then
    echo "All mods loaded"
else
    # If not, check if mods were loaded at all
    egrep "$PREFIX\[Notification\] Found [0-9] mods .*" $LOGFILE

    if [ $? -ne 0 ]; then
        echo "No mods loaded at all"
        exit 1
    fi

    echo "Some mods loaded, but with errors"
fi


# Line in log: 21.1.2021 22:55:00 [Notification] Mods, sorted by dependency: game, creative, survival, bricklayers

# Check if all mods were loaded successfully
echo "Checking if all mods were loaded..."
egrep "$PREFIX\[Notification\] Mods, sorted by dependency: game, creative, survival, $MODNAME" $LOGFILE

if [ $? -eq 0 ]; then
    echo "Mods loaded successfully"
else
    echo "Mods were not loaded successfull"
    exit 1
fi


# Line in log: 21.1.2021 22:55:09 [Warning] Failed resolving a blocks itemdrop or smeltedstack with code bricklayers:resin in cooking recipe bricklayers:recipes/cooking/glueportion.json

# Check if there are warnings/errors regarding the mod
echo "Checking if there are warnings or errors regarding the mod..."
egrep "$PREFIX\[(Warning|Error)\] .*$MODNAME.*" $LOGFILE

if [ $? -eq 0 ]; then
    echo "There are some warnings/errors regarding the mod."
    # TODO: Set to exit code 1 or think of a different solution on how to monitor warnings
    #exit 1
fi

echo "There are no warnings/errors regarding the mod"
