#!/usr/bin/perl

# Test that the .zip file and its contents look sane

use Test::More;
use JSON;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use strict;
use warnings;

# load the test helper library
use lib 'test/lib';
use VSJSON;

# we expect the build stage to put the mod here
use constant DIST_DIR => 'dist';

# We run at least so many tests:
# 4 basic tests + 2 tests for each tested file
my $tests_run = 4 + 6 * 2;

# check that the right ZIP was generated
my $modinfo = VSJSON::parse_json( 'modinfo.json' );

my $zipfile = DIST_DIR . '/' . $modinfo->{modid} . '-' . $modinfo->{version} . '.zip';

ok (-f $zipfile, 'Zip file exists');
ok (-s $zipfile > 200, 'Zip file is at least 200 byte');
like (`file -i '$zipfile'`, qr/application\/zip/, 'File is actually a ZIP file');

my $modzip = Archive::Zip->new();
ok ($modzip->read( $zipfile ) == AZ_OK, 'Could open the ZIP file');

# check that certain files are present in the archive
for my $file ( qw/
	modinfo.json modicon.png
	CHANGELOG.md
	CREDITS.md
	LICENSE
	README.md
	/)
  {
  my $member = $modzip->memberNamed( $file );
  # is there a member with the name "$file"?
  is (ref($member), 'Archive::Zip::ZipFileMember', "Archive contains $file");
  # and has it the same uncompressed size as the original file?
  my $fsize = -s $file;
  is ($member->{uncompressedSize}, $fsize, "$file is $fsize bytes");
  }

# check that there are no ".something" or (.desktop) files present in the archive
my @members = $modzip->members();
for my $member (@members)
  {
  my $filename = $member->{fileName};
  unlike ($filename, qr/^(\.|\.desktop)/, "'$filename' looks good");
  $tests_run ++;
  }
done_testing( $tests_run );

# EOF

