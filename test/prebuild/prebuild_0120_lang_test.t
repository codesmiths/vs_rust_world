#!/usr/bin/perl

# Test that language files do not contain double entries

use Test::More;
use JSON;
use File::Find;
use warnings;
use strict;
use utf8;
use Encode qw/decode/;

# load the test helper library
use lib 'test/lib';
use VSJSON;

my $number_of_tests_run = 0;

use constant TEST_PATH => 'assets/';

my $info = VSJSON::gather_json_info( TEST_PATH );
my $modid = $info->{modid};
like ($modid, qr/^[a-z]+[a-z0-9-]+$/, "modid $modid looks valid");
$number_of_tests_run ++;

my @LANGUAGES;

# define the subroutine to gather all JSON files
sub is_json {
  push @LANGUAGES, $File::Find::name
	if -f $File::Find::name && $File::Find::name =~ /\.json\z/;
};

# Gather all files
find({ wanted => \&is_json, no_chdir => 1 }, TEST_PATH . $modid . '/lang/');

use constant QR_VALID_LINE	=> qr/^[\s\t]*"([a-z]+[a-z0-9-]+):([^"]+)"[\s\t]*:[\s\t]*"(([^"]|\\")+)",?[\s\t]*\z/;
use constant QR_SINGLE_COLON	=> qr/^[\s\t]*"[^"]*:[^"]*:/;
use constant QR_ENGLISH		=> qr/\b(when|other|ground|powder|crushed|planter|vessel|brick)\b/i;

my %english;

binmode STDOUT, ':utf8';
binmode STDERR, ':utf8';
# Test each file, sorted by name
# - We sort them so that english comes first and we can record all English strings
# - Then for each other language, we can detect untranslated strings
FILE:
for my $file (sort {
	my $a_score = ($a =~ /\/en\.json/) ? 1000 : 1;
	my $b_score = ($b =~ /\/en\.json/) ? 1000 : 1;
        $b_score <=> $a_score || $a cmp $b
	} @LANGUAGES)
  {

  # read exceptions for translations that should override game translations
  my $exception_file = $file; $exception_file =~ s/assets\/$modid\/lang\//test\/prebuild\/lang_exceptions\//;
  my $exceptions = { entries => [] };

  $exceptions = VSJSON::parse_json( $exception_file ) if -f $exception_file;

  open (my $FH, '<', $file) or fail("$file - $!"), next;

  # parse the file, recording every entry
  my %entries;
  my $linenr = -1;
  my $saw_open = 0;
  my $saw_close = 0;

  LINE:
  while (my $rawline = <$FH>)
    {
    $linenr++;

    my $line = decode('UTF-8', $rawline, Encode::FB_CROAK);

    # skip empty lines
    next LINE if $line =~ /^[\s\t\r\n]+\z/;

    $line =~ s/[\r\n]+//;

    $saw_open ++ if $line =~ /^[\s\t]*[{][\s\t]*\z/;
    $saw_close ++ if $line =~ /^[\s\t]*[}][\s\t]*\z/;

    # skip "{" or "}"
    next LINE if $line =~ /^[\s\t]*[{}][\s\t]*\z/;

    if ($line =~ QR_SINGLE_COLON)
      {
      unlike($line, QR_SINGLE_COLON, "$file line $linenr: Invalid double ':'");
      $number_of_tests_run ++;
      next FILE;
      }
    # "modid:item-something":               "Some text"
    if ($line !~ QR_VALID_LINE)
      {
      like($line, QR_VALID_LINE, "$file line $linenr: invalid entry syntax");
      $number_of_tests_run ++;
      next FILE;
      }

    my ($domain, $entry, $translation) = ($1,$2,$3);
    if ($domain !~ /^(game|$modid|block-$modid)$/)
      {
      like($domain, qr/^(game|$modid)$/, "$file line $linenr: Entry must refer to game: or ${modid}: but is '$domain:$entry'");
      $number_of_tests_run ++;
      next LINE;
      }

    if (exists $entries{$entry})
      {
      my ($other_domain, $prev) = @{ $entries{$entry} };
      # #328: exception for incontainer liquid names
      # if these are defined in game and later in the modid, this is okay
      # Even in 1.17.5, these double translations are nec.
      if ($domain eq $modid && $other_domain eq 'game' && $entry =~ /incontainer/)
	{
	next LINE;
	}
      is ($prev, 0, "$file line $linenr: Entry $entry already defined with domain $other_domain in line $prev");
      $number_of_tests_run ++;
      next LINE;
      }

    if ($file =~ /\ben\.json/)
      {
      # For english, store the entry
      $english{"$domain:$entry"} = $translation;
      next LINE;
      }
    else
      {
      # For non-english, bail if the entry does not exist in the main english file
      my $exists = exists $english{"$domain:$entry"};
      if (!$exists)
        {
	# ignore exceptions
	my $exception = exists $exceptions->{overrides}->{"$domain:$entry"};
	if (!$exception)
	  {
	  is ($exists, 1, "$file line $linenr: Entry $entry is missing in English, left-over translation?");
          $number_of_tests_run ++;
	  }
	}
      else
        {
        # For non-english, bail if the entry is the same
	if ($english{"$domain:$entry"} eq $translation)
	  {
	  my $exception = exists $exceptions->{same}->{"$domain:$entry"};
	  if (!$exception)
	    {
	    isnt ($english{"$domain:$entry"}, $translation, "$file line $linenr: Entry $entry is untranslated");
            $number_of_tests_run ++;
	    }
	  }
	}

      }

    $entries{$entry} = [ $domain, $linenr ];
    }
  close ($FH);
  is ( $saw_open, 1, "File $file has no double entries, contains '{'");
  is ( $saw_close, 1, "File $file has no double entries, contains '}'");
  $number_of_tests_run += 2;

  # DEBUG: uncomment this to get a list of all missing translations
  if (3 > 5 && $file !~ /\ben\.json/)
    {
    my @missing;
    # go through all english entries
    for my $entry (sort keys %english)
      {
      # "game:foo" => "game", "foo"
      my $bare_entry = $entry;
      $bare_entry =~ s/^([a-zA-Z0-9-]+)://; my $domain = $1;
      #print STDERR "Comparing '$entry' '$english{$entry}' vs. bare: '$bare_entry' '$entries{$bare_entry}'\n"; sleep(1);
      push @missing, "\t\"$entry\":\t\t\t\"$english{$entry}\""
	unless exists $entries{$bare_entry};
      }
    if (@missing > 0)
      {
      open (my $FHM, '>', "${file}.missing") or fail("${file}.missing - $!"), next;
      print $FHM "{\n";
      while (my $line = shift @missing)
	{
	# skip the "," on the last line
	print $FHM $line . (@missing == 0 ? '' : ',') . "\n";
	}
      print $FHM "}\n";
      close $FHM;
      }
    }
  }

done_testing( $number_of_tests_run );
