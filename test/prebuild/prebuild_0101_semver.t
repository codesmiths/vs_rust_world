#!/usr/bin/perl

# Test that version strings are valid semantic versions

use Test::More;
use JSON;
use File::Find;
use warnings;
use strict;

# load the test helper library
use lib 'test/lib';
use VSJSON;

sub valid_sem_ver($);

my $number_of_tests_run = 0;

my $modinfo = VSJSON::parse_json('modinfo.json');

valid_sem_ver( $modinfo->{version} );
for my $dep (sort keys %{$modinfo->{dependencies}})
  {
  valid_sem_ver( $modinfo->{dependencies}->{$dep} );
  }
done_testing( $number_of_tests_run );

#############################################################################
# Test sematic version formatting

sub valid_sem_ver($)
  {
  my ($ver) = @_;

  like ($ver, qr/^[0-9]+\.[0-9]+\.[0-9]+(-(rc|pre)\.[0-9]+)?$/, "'$ver' is a valid sematic version string");
  $number_of_tests_run ++;
  }

# EOF
