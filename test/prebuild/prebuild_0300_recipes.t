#!/usr/bin/perl

# Test that recipes conform to some minimum standards:
#
# * grid recipes: width/height should match the recipe pattern
# * output or returnedStack codes should only reference valid codes

use Test::More;
use warnings;
use strict;
use utf8;

# load the test helper library
use lib 'test/lib';
use VSJSON;

# subroutine definitions
sub check_code($ $ $ $ $);

# The global number of tests we did run
my $number_of_tests_run = 0;

use constant TEST_PATH => 'assets/';
use constant DEBUG => 0;

# Step 1: Gather all JSON info from blocktypes, itemtypes
# Step 2: Test recipes

my $info = VSJSON::gather_json_info( TEST_PATH );
my $modid = $info->{modid};
like ($modid, qr/^[a-z]+[a-z0-9-]+$/, "modid $modid looks valid");
$number_of_tests_run ++;

my $valid_codes = $info->{codes};		# contains all valid "base" codes like "glazedbricks"
my $valid_variants = $info->{variants};		# and all their variants like "glazedbricks-milky-red"

print "# Found " . (scalar keys %$valid_codes) . " codes and " . (scalar keys %$valid_variants) . " variants.\n";

# Test grid recipes for valid codes, and pattern size, sorted by name
for my $json_file (sort @{ $info->{grid_recipes} } )
  {
  my $json = VSJSON::parse_json($json_file);
  ok (ref($json), "$json_file has valid JSON");
  $number_of_tests_run ++;

  # #357: If the recipe is a single item, complain.
  is (ref($json), 'ARRAY', "Recipes need to be a list, not a single item");
  $number_of_tests_run ++;

  last if ref($json) ne 'ARRAY';

  my $recipe_index = 0;
  # for all recipes in this file
  for my $recipe (@$json)
    {
    $recipe_index ++;
    my $name = defined $recipe->{code} ? "recipe $recipe->{code}" : "$json_file: recipe $recipe_index";
    # check that the recipe pattern matches height/width
    my $pattern = $recipe->{ingredientPattern};

    isnt ($pattern, undef, "$name: has a pattern defined");
    $number_of_tests_run ++;
    next unless defined $pattern;

    like ($pattern, qr/[A-Za-z_, ]+/, "$name: Pattern looks valid");
    $number_of_tests_run ++;

    # compute the height + width from the pattern
    my $height = 0;
    my $width = 0;
    my $first_width;
    # do all lines have the same width?
    my $all_the_same = 1;
    for my $line (split /,/, $pattern)
      {
      $height++;
      my $line_width = length($line);
      $width = $line_width if $line_width > $width;
      if (!defined $first_width)
        {
	$first_width = $width;
	}
      else
	{
        is ($line_width, $first_width, "Grid $name: Line $height must be $width characters");
        $number_of_tests_run ++;
	}
      }
    ok ($height >= 1 && $height <= 3 , "Grid $name: Pattern height looks sensible");
    ok ($width >= 1 && $width <= 3 , "Grid $name: Pattern width looks sensible");

    is ($height, $recipe->{height}, "Grid $name: Pattern height is correct");
    is ($width, $recipe->{width}, "Grid $name: Pattern width is correct");

    my $pattern_checks = 0;
    # check that each recipe ingredient is actually defined
    for my $char (split (//, $pattern))
      {
      # ignore commas, underlines and spaces as separators
      next if $char =~ /[,_ ]/;
      # speed up testing by only running the test if it would fail
      next if exists $recipe->{ingredients}->{ $char };

      is (exists $recipe->{ingredients}->{ $char } ? 1 : 0, 1, "Pattern $name: ingredient '$char' is defined");
      $pattern_checks ++;
      }
    $number_of_tests_run += 4 + $pattern_checks;

    # 1.16/1.17: check that no ingredient defines liquid attributes, these should be on the recipe itself
    my $attribute_checks = 0;
    for my $code (keys %{ $recipe->{ingredients} })
      {
      my $thing = $recipe->{ingredients}->{$code};
      if (exists $thing->{attributes} && ref($thing->{attributes}) eq 'HASH')
        {
	if (exists $thing->{attributes}->{liquidContainerProps})
	  {
	  is (exists $thing->{attributes}->{liquidContainerProps} ? 1 : 0, 0, "Pattern $name: ingredient '$code' has no liquidContainerProps");
	  $attribute_checks ++;
	  }
	}
      }
    $number_of_tests_run += $attribute_checks;
    }
  }

# Now test all recipes for valid codes, sorted by name
for my $json_file (sort @{ $info->{grid_recipes} }, @{ $info->{other_recipes} } )
  {
  my $json = VSJSON::parse_json($json_file);
  ok (ref($json), "$json_file has valid JSON");
  $number_of_tests_run ++;

  # Recipe can also be single recipes, convert them into a list
  my $recipes = ref($json) eq 'ARRAY' ? $json : [ $json ];

  # for all sub-recipes
  for my $recipe (@$recipes)
    {
    my $name = $recipe->{code} // 'unknown';

    # check that output and returnedBlock match existing codes
    check_code( $info, $json_file, $name, $recipe->{output}->{code}, $recipe->{output}->{type} );

    if (exists $recipe->{ingredients})
      {
      my $ingredients = $recipe->{ingredients};
      my @ingredient_list = ref($ingredients) eq 'HASH' ? sort keys %$ingredients : @$ingredients;
      for my $ingredient (@ingredient_list)
        {
	my $n = $name;
	my $i = $ingredient;
	if (!ref($ingredient))
	  {
	  # for things like "M: { .. }"
	  $n = "$name ($ingredient)";
	  $i = $ingredients->{$i};
	  }
        check_code( $info, $json_file, $n, $i->{code}, $i->{type} );
        check_code( $info, $json_file, $n, $i->{returnedStack}->{code}, $i->{returnedStack}->{type} )
		if exists $i->{returnedStack} && ref($i->{returnedStack});
        }
      }
    else
      {
      # it must be a clay forming recipe
      ok (exists $recipe->{ingredient}, "Clayforming pattern $name is ok");
      $number_of_tests_run ++;
      }
    }
  }

done_testing( $number_of_tests_run );

# End of code

#############################################################################
# Subroutines

sub check_code($ $ $ $ $)
  {
  # check that the given code is a valid one
  my ($info, $file, $name, $code, $type) = @_;

  # If the namespace of this code is something other than "$modid:" or "game:",
  # then check if the name space matches the other mod id in the file path
  # for compatibility recipes:
  # 'assets/bricklayers/compatibility/moremetals/recipes/' => $othermod => 'moremetals'
  my $othermod = undef;
  $othermod = $1 if $file =~ /assets\/[a-z0-9]+\/compatibility\/([a-z0-9_-]+)\/recipes/;

  # #355 - compatibility recipes with other mods
  if ($othermod)
    {
    like ($code, qr/^($modid|game|$othermod):/, "$file: $code is in compatibility mod namespace");
    $number_of_tests_run ++;

    # we cannot check the acual code in other mod namespaces, so skip the next test
    return;
    }

  my $rc = VSJSON::check_code($info, $file, $name, $code, $type);

  is ($rc, undef, "$file:$code is valid");
  $number_of_tests_run ++;
  }

