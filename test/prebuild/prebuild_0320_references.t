#!/usr/bin/perl

# Test that JSON items or blocks references for textures, shapes, or other items/blocks
# point to existing objects.

use Test::More;
use warnings;
use strict;
use utf8;

# load the test helper library
use lib 'test/lib';
use VSJSON;

# subroutine definitions
sub test_texture($ $ $ $ $ $ $ $);
sub _generate_alternatives($ $ $ $ $);
sub check_code($ $ $ $ $ $);
sub test_shape($ $ $ $ $ $);
sub test_liquid_properties($ $ $ $ $);

# The global number of tests we did run
my $number_of_tests_run = 0;

# test each file only once to speed up the tests
my $files_tested = {};

use constant TEST_PATH => 'assets/';
use constant DEBUG => 0;

# Step 1: Gather all JSON info
# Step 2: Test JSON references

my $info = VSJSON::gather_json_info( TEST_PATH );
my $modid = $info->{modid};
like ($modid, qr/^[a-z]+[a-z0-9-]+$/, "modid $modid looks valid");
$number_of_tests_run ++;

# Test JSON references to be valid
for my $json_file (sort @{ $info->{itemtypes} }, @{ $info->{blocktypes} } )
  {
  my $json = VSJSON::parse_json($json_file);

  ok (ref($json), "$json_file has valid JSON");
  $number_of_tests_run ++;

  # we expect only one item/block per JSON file
  is (ref($json), 'HASH', 'Expected an object, not an array');
  $number_of_tests_run ++;

  print "# $json_file\n" if DEBUG > 0;

  # build a list of valid states, so we can check textures referencing them
  my $states = {};
  if (exists $json->{variantgroups})
    {
    for my $state (@{$json->{variantgroups}})
      {
      my ($scode, $sstates) = VSJSON::generate_states_from_json($json_file, $state,
	      # remove skipped variants to be skipped, so we do not try to check them
	      $json->{skipVariants} // $json->{skipvariants} // []);
      # if we have some variants, remember them
      $states->{$scode} = $sstates if @$sstates > 0;
      }
    }
  # for all properties in the object
  for my $key (sort keys %$json)
    {
    if ($key eq 'texture')
      {
      test_texture( \$number_of_tests_run, $json_file, $states, $key, $json->{$key}, 0, undef, {} );
      }
    elsif ($key eq 'textures')
      {
      for my $entry (sort keys %{ $json->{$key} })
        {
        test_texture( \$number_of_tests_run, $json_file, $states, "$key $entry", $json->{$key}->{$entry}, 0, undef, {} );
	}
      }
    elsif ($key eq 'texturesByType')
      {
      my $done_states = {};
      for my $entry (sort {
		# longest entries first
		length($b) <=> length($a) or
		$b cmp $a
		}
		keys %{ $json->{$key} })
        {
	# extract the type name from the key ("*-obsidian-*" => "obsidian")
	my $type = $entry; $type =~ s/[\*-]//g;
	# build a list, for normal entries, there is only one type like "red"
	my @types = ($type);
	# @.*-(white|paleblue|palegreen|palered)-.*
	if ($entry =~ /\@.*\(([^\)]+)-?/)
	  {
	  # for these, we check all alternative types like white, paleblue etc.
	  my $types = $1;
	  @types = split /\|/, $types;
	  # print STDERR "Found multiple types '$types' '", join( ", ", @types), "'\n" if DEBUG;
	  }
        for my $text (sort keys %{ $json->{$key}->{$entry} })
	  {
	  for my $type (@types)
	    {
	    test_texture( \$number_of_tests_run, $json_file, $states, "$key $entry $text", $json->{$key}->{$entry}->{$text}, 0, $type, $done_states );
	    $done_states->{$type} = undef unless $type eq '';
	    }
	  }
	}
      }
    elsif ($key eq 'shape')
      {
      test_shape( \$number_of_tests_run, $info, $json_file, $states, $key, $json->{$key} );
      }
    elsif ($key eq 'shapeByType')
      {
      for my $entry (sort {
		# longest entries first
		length($b) <=> length($a) or
		$b cmp $a
		}
		keys %{ $json->{$key} })
        {
	test_shape( \$number_of_tests_run, $info, $json_file, $states, "$key $entry", $json->{$key}->{$entry} );
	}
      }
    elsif ($key eq 'attributes')
      {
      my $att = $json->{$key};
      my $text = 'inContainerTexture';
      if (exists $att->{$text})
	{
	  test_texture( \$number_of_tests_run, $json_file, $states, "$key attributes $text", $att->{$text}, 0, undef, {} );
	}
      test_liquid_properties( \$number_of_tests_run, $info, $json_file, $states, $att->{liquidContainerProps})
	if exists $att->{liquidContainerProps};
      }
    elsif ($key eq 'attributesByType')
      {
      for my $entry (sort {
		# longest entries first
		length($b) <=> length($a) or
		$b cmp $a
		}
		keys %{ $json->{$key} })
        {
	my $att = $json->{$key}->{$entry};
	next unless ref($att);
	if (exists $att->{mealBlockCode})
	  {
	  check_code( \$number_of_tests_run, $info, $json_file, "$key $entry mealBlockCode", $att->{mealBlockCode}, 'block');
	  }
	my $text = 'inContainerTexture';
	if (exists $att->{$text})
	  {
	  test_texture( \$number_of_tests_run, $json_file, $states, "$key $entry $text", $att->{$text}, 0, undef, {} );
	  }
        test_liquid_properties( \$number_of_tests_run, $info, $json_file, $states, $att->{liquidContainerProps})
		if exists $att->{liquidContainerProps};
	}
      }
    elsif ($key eq 'combustiblePropsByType')
      {
      for my $entry (sort {
		# longest entries first
		length($b) <=> length($a) or
		$b cmp $a
		}
		keys %{ $json->{$key} })
        {
	my $att = $json->{$key}->{$entry};
	if (ref($att) && exists $att->{smeltedStack})
	  {
	  my $smelted = $att->{smeltedStack};
	  ok (ref($smelted), "$key -> smeltedStack is a reference");
	  my $type = $att->{smeltedStack}->{type} // 'unknown';
	  like ($type, qr/^(block|item)$/, "$key -> smeltedStack -> $type is a valid type");
	  $number_of_tests_run += 2;
	  check_code( \$number_of_tests_run, $info, $json_file, "$key $entry smeltedStack", $smelted->{code}, $type);
	  }
	}
      }
    }
  }

done_testing( $number_of_tests_run );

#############################################################################
# Subroutines

sub _generate_alternatives($ $ $ $ $)
  {
  # given a filename with templates like "{foo}/{bar}/{baz}" and a list
  # of states for foo like [a,b,c] it will generate
  # "a/{bar}/{baz}"
  # "b/{bar}/{baz}"
  # "c/{bar}/{baz}"
  # If $onlytype is set, we ignore all other states
  my ($json_file, $states, $template, $onlystate, $donestates) = @_;

  # if the template contains no placeholders, exit
  return $template if $template !~ /\{[^\}]+\}/;

  # 1: gather all templates
  my @templates;
  {
    my $gather = $template; $gather =~ s/\{([^\}]+)\}/ push @templates, $1; /eg;
  }
  print "# Saw templates: ", join(" ", @templates), "\n" if DEBUG;

  # replace all occurances of the first template with their states
  my $tpl = shift @templates;
  die ("Cannot find state $tpl in $json_file") unless exists $states->{$tpl};

  my @output;
  for my $state (@{ $states->{$tpl} })
    {
    # if we are asked to only produce "obsidian" for {rock}, ignore all other states
    next if $onlystate && $state ne $onlystate;

    if (DEBUG && $template =~ /hardened/)
      {
      print "# at $state\n";
      require Data::Dumper; print Data::Dumper::Dumper($donestates);
      }

    # if we not asked to only produce "obsidian" for {rock}, ignore already done states
    next if exists $donestates->{$state};

    my $new = $template;
    $new =~ s/\{$tpl\}/$state/;
    print "# Generated $new from $template ($tpl)\n" if DEBUG;
    push @output, $new;
    }

  @output;
  }

sub test_texture($ $ $ $ $ $ $ $)
  {
  my ($tests, $file, $states, $name, $value, $level, $onlystate, $donestates) = @_;

  is (ref($value), 'HASH', "$file: $name is a reference");
  $$tests ++;

  # "texturesByType": {
  #  "*-raw-*": {
  #  		"ceramic": { "base": "game:block/clay/ceramic" },
  #             "top":	   { "base": "bricklayers:block/ceramic/glazed/{state}/{type}/{color}",
  #			     "overlays": [ "bricklayers:block/ceramic/glazed/pattern/{pattern}" ] },

  # we expect only "alternates", "base" or "overlays" or "rotation"
  # "alternates" should only present if we are at level 0
  my $found = 0;
  for my $key (sort keys %{$value})
    {
    $found ++ if $key =~ /^(base|overlays|rotation|alpha)/;
    $found ++ if $level == 0 && $key eq 'alternates';
    }
  is (scalar keys %$value, $found, "$file: Texture reference $name has 'base' and only optional alternates or overlays");
  $$tests ++;
  # speed up testing: only check alpha if it is actually used
  if (defined $value->{alpha})
    {
    my $alpha = $value->{alpha};
    is (int($alpha), $alpha, "$file: Texture alpha is an integer");
    ok ($alpha >= 0 && $alpha <= 255, "$file: Texture alpha is between 0 and 255");
    $$tests += 2;
    }

  # if this texture has alternates, test these, too
  if (exists $value->{alternates})
    {
    is (ref($value->{alternates}), 'ARRAY', "$file: $name alternates is an array");
    $$tests ++;
    my $idx = 0;
    for my $alternate (@{ $value->{alternates} })
      {
      test_texture($tests, $file, $states, "$name alternate $idx", $alternate, $level + 1, $onlystate, $donestates);
      $idx++;
      }
    }

  if (exists $value->{rotation})
    {
    like($value->{rotation}, qr/^(0|90|180|270)/, "$file: Texture reference $name has valid rotation");
    $$tests ++;
    }

  my @todo_entries = ($value->{base});
  push @todo_entries, @{ $value->{overlays} } if ref($value->{overlays});

  ALTERNATIVE:
  for my $tfile (@todo_entries)
    {
    # see if we can test the reference file to exist

    # files in the game: domain cannot be tested
    next ALTERNATIVE unless $tfile =~ /^$modid:(.*)/;

    $tfile = "assets/$modid/textures/$1.png";
    # turn "red*.png" into "red1.png" (we test only that at least the first variant exists)
    $tfile =~ s/\*/1/;

    # did we test this file already?
    next ALTERNATIVE if exists $files_tested->{$tfile};

    if ($tfile !~ /\{/)
      {
      # we have no states mentioned
      is (-s $tfile > 100 ? 1 : 0, 1, "File $tfile has at least 100 bytes (referenced in $file)");
      $files_tested->{$tfile} = undef;		# remember that we tested that file
      $$tests++;

      next ALTERNATIVE;
      }

    # we have templates like "{pattern}" in the filename
    my @todo = $tfile;
    FILE:
    while (my $current = shift @todo)
      {
      # if this entry contains a {} template, generate a list of new
      # strings from it.
      if ($current =~ /\{[^\}]+\}/)
	{
	my @new = _generate_alternatives($file, $states, $current, $onlystate, $donestates);
	push @todo, @new;
	next FILE;
	}

      next FILE if exists $files_tested->{$current};

      # otherwise, just test this entry as a filename
      is ((-s $current // 0) ? 1 : 0, 1, "File $current has at least 100 byte (referenced from $file).");
      $files_tested->{$current} = undef;		# remember that we tested that file
      $$tests++;
      } # end for all variants of this entry

    } # end for this entry

  }

sub check_code($ $ $ $ $ $)
  {
  # check that the given code is a valid one
  my ($tests, $info, $file, $name, $code, $type) = @_;

  my $rc = VSJSON::check_code($info, $file, $name, $code, $type);

  is ($rc, undef, "$file:$code is valid");
  $$tests ++;
  }


sub test_shape_name($ $ $ $ $ $)
  {
  my ($tests, $info, $file, $states, $name, $code) = @_;

  like ($code, qr/^(game|$modid):/, "$file: ${name}'s shape reference $code has a valid modid");
  $$tests ++;

  # cannot test game: references yet
  return unless $code =~ /^$modid:/;

  # cannot test references with "{somestate}" in it yet
  return if $code =~ /\{/;

  # "modid:foo/bar" => "assets/modid/shapes/foo/bar.json"
  $code =~ s/^$modid:/assets\/$modid\/shapes\//;
  $code .= '.json';

  # check that "assets/$modid/shapes/$code" exists
  is (-f $code, 1, "Shape $code exists");
  $$tests ++;
  }


sub test_shape($ $ $ $ $ $)
  {
  my ($tests, $info, $file, $states, $name, $value) = @_;

  is (ref($value), 'HASH', "$file: $name is a reference");
  $$tests ++;

  #	"shapeByType": {
  #		"*-north": { "base": "game:block/clay/crock/base", "rotateY": 0 },
  # or:
  # 	"shape": { "base": "game:item/liquid" }

  # we expect only "base", or rotateX, rotateY, rotateZ
  my $found = 0;
  my $code = 'notfound';
  for my $key (sort keys %{$value})
    {
    $found ++ if $key =~ /^(base|rotate[XYZxyz]|scale)$/;
    $code = $value->{$key} if $key eq 'base';
    }
  my $cnt = scalar keys %$value;
  is ($found, $cnt, "$file: Shape reference $name has 'base' and only optional rotation or scale");
  if ($cnt != $found)
    {
      require Data::Dumper;
      print STDERR Data::Dumper::Dumper($value);
    }

  $$tests ++;

  test_shape_name( $tests, $info, $file, $states, $name, $code );
  }


sub test_liquid_properties($ $ $ $ $)
  {
  my ($tests, $info, $file, $states, $att) = @_;

  is (ref($att), 'HASH', "$file: attributes->liquidContainerProps is a reference");
  $$tests ++;

  for my $shapeloc (qw/emptyShapeLoc opaqueContentShapeLoc liquidContentShapeLoc/)
    {
    is (ref($att->{$shapeloc}), '', "$file: attributes->liquidContainerProps->$shapeloc is not reference");
    $$tests ++;
    test_shape_name( $tests, $info, $file, $states, $shapeloc, $att->{$shapeloc} );
    }

  #  test liquid amounts
  for my $key (qw/capacityLitres transferSizeLitres liquidMaxYTranslate/)
    {
    ok ( $att->{$key} > 0, "$file: $key > 0");
    $$tests ++;
    }
  ok ( $att->{capacityLitres} >= $att->{transferSizeLitres}, "$file: capacityLitres >= transferSizeLitres");
    $$tests ++;
  }


# End of code

