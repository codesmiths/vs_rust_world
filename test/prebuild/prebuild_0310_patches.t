#!/usr/bin/perl

# Test that JSON patches conform to some minimum standards:
#
# * contain valid JSON
# * contain a file: entry
# * have the right op and their matching fields

use Test::More;
use warnings;
use strict;
use utf8;

# load the test helper library
use lib 'test/lib';
use VSJSON;

# The global number of tests we did run
my $number_of_tests_run = 0;

use constant TEST_PATH => 'assets/';
use constant DEBUG => 0;

# Step 1: Gather all JSON info
# Step 2: Test JSON patches

my $info = VSJSON::gather_json_info( TEST_PATH );
my $modid = $info->{modid};
like ($modid, qr/^[a-z]+[a-z0-9-]+$/, "modid $modid looks valid");
$number_of_tests_run ++;

# Test patches to be valid
for my $json_file (sort @{ $info->{patches} } )
  {
  my $json = VSJSON::parse_json($json_file);

  ok (ref($json), "$json_file has valid JSON");
  $number_of_tests_run ++;

  my $patches = ref($json) eq 'ARRAY' ? $json : [ $json ];

  my $patch_index = 0;
  # for all patches in this file
  for my $patch (@$patches)
    {
    $patch_index ++;

    my $name = "$json_file patch $patch_index";
    isnt ($patch->{file}, undef, "$name: has a file defined");

    # if the patch is in the compatibility folder, use the modid from the path
    my $othermod = $modid;
    # assets/bricklayers/compatibility/moremetals/patches/ => moremetals
    $othermod = $1 if $json_file =~ /assets\/[a-z0-9]+\/compatibility\/([a-z0-9_-]+)\/patches/;

    like ($patch->{file}, qr/^(game|$modid|$othermod):/, "$name: patches a valid namespace");

    like ($patch->{op}, qr/^(add|addeach|remove|replace)$/, "$name: has a valid op");
    isnt ($patch->{path}, undef, "$name: defines a path");

    my $op = $patch->{op} // '';
    if ($op eq 'add' || $op eq 'replace')
      {
      isnt ($patch->{value}, undef, "$name: adds a value");
      }
    elsif ($op eq 'remove')
      {
      is ($patch->{value}, undef, "$name: does not define a value");
      }
    elsif ($op eq 'addeach')
      {
      is (ref($patch->{value}), 'ARRAY', "$name: value needs to be an ARRAY");
      }
    $number_of_tests_run += 5;
    }
  }

done_testing( $number_of_tests_run );

# End of code

