# A perl library with utility functions to parse, analyze and test
# Vintage Story specific JSON4 files.

package VSJSON;

use JSON;
use File::Find;
use warnings;
use strict;
use utf8;
use Encode;

# We assume that codes and variants are unique across items and blocks,
# e.g. there cannot be an item and a block with the same code.

use constant DEBUG => 0;

our $VERSION = '0.0.6';

#############################################################################
# Public routines

sub parse_json($);
sub gather_json_info($);
sub generate_states_from_json($ $ $);

#############################################################################
# Internal code:

sub gather_json();

# global lists for File::Find
my @RECIPES;
my @GRID_RECIPES;
my @PATCHES;
my @JSONS;
my @ITEMS;
my @BLOCKS;
my @ENTITIES;

# Define the subroutine to gather all relevant JSON files
sub gather_json() {
  # a shortcut
  my $fname = $File::Find::name;

  # ignore non-JSON files
  return unless	-f $fname && $fname =~ /\.json\z/;

  if ($fname =~ /recipes\//)
    {
    if ($fname =~ /\/grid\//)
      {
      push @GRID_RECIPES, $File::Find::name;
      }
    else
      {
      push @RECIPES, $File::Find::name;
      }
    }
  elsif ($fname =~ /patches\//)
    {
    push @PATCHES, $File::Find::name;
    }
  elsif ($fname =~ /(blocktypes|itemtypes)\//)
    {
    push @JSONS, $fname;
    push @ITEMS, $fname if $fname =~ /itemtypes\//;
    push @BLOCKS, $fname if $fname =~ /blocktypes\//;
    }
  elsif ($fname =~ /entities\//)
    {
    push @ENTITIES, $File::Find::name;
    }
};

#############################################################################
# Routine to read and parse a JSON file into memory in one go

sub parse_json($)
  {
  # load a file and parse it from JSON into memory
  my ($json_file) = @_;

  open (my $FH, '<', $json_file) or return undef;
  #  binmode $FH, ':utf8';
  local $/ = undef;	# slurp all data
  my $data = <$FH>;

  # try to decode the data as UTF-8
  my $utf8;
  eval {
    $utf8 = encode( 'UTF-8', $data);
  };
  return unless $utf8;

  # try to parse the JSON
  my $json;
  eval {
    $json = decode_json($data);
  };
  $json;
  }

sub generate_states_from_json($ $ $)
  {
  my ($file, $state, $skip_rules) = @_;

  # if this state loads properties, emulate it so we can get the full list:
  if ($state->{loadFromProperties})
    {
    my $states = $state->{states};
    my $lstate;
    $lstate = { "code" => "verticalorientation", "states" => [ "up", "down" ] }
	if $state->{loadFromProperties} =~ /^(game:)?abstract\/verticalorientation$/;
    $lstate = { "code" => "horizontalorientation", "states" => [ "north", "east", "south", "west" ] }
	if $state->{loadFromProperties} =~ /^(game:)?abstract\/horizontalorientation$/;
    $lstate = { "code" => "rock",
	  "states" => [	"andesite", "chalk", "chert", "conglomerate", "limestone", "claystone", "granite",
			"sandstone", "shale", "basalt", "peridotite", "phyllite", "slate", "bauxite" ] }
	if $state->{loadFromProperties} =~ /^(game:)?block\/rock$/;
    $lstate = { "code" => "metal",
	  "states" => [ "bismuth", "bismuthbronze", "blackbronze", "brass", "chromium", "copper", "gold",
			"iron", "meteoriciron", "lead", "molybdochalkos", "platinum", "rhodium", "silver",
			"stainlesssteel", "steel", "tin", "tinbronze", "titanium", "uranium", "zinc" ] }
	if $state->{loadFromProperties} =~ /^(game:)?block\/metal$/;
    $lstate = { "code" => "wood",
	  "states" => [	"birch", "oak", "maple", "pine", "acacia", "kapok", "baldcypress", "larch",
			"redwood", "ebony", "walnut", "purpleheart" ] }
	if $state->{loadFromProperties} =~ /^(game:)?block\/wood$/;
    $state = $lstate if $lstate;
    if ($states)
      {
      # merge the original list into the newly loaded list
      push @{$state->{states}}, @$states;
      }
    }

  # go through the new-to-add list and remove any variants we should skip
  my @final_states;
  for my $state (@{$state->{states}})
    {
    my $skip = 0;
    RULE:
    for my $skip_rule (@$skip_rules)
      {
      # transform the skip rule into a regexp
      my $qr = $skip_rule;
      if ($skip_rule =~ /^\@/)
        {
	# "@.*-white" => qr/.*-white/
        $qr =~ s/^\@//;
	}
      else
        {
	# "*-white" => qr/.*-white/
        $qr =~ s/\*/\.\*/g;
	}
      if ($state =~ /$qr/)
        {
	print STDERR "Removing state $state from variants due to $skip_rule matching.\n";
	$skip = 1;
	last RULE;
	}
      }
    # should we keep this state?
    push @final_states, $state unless $skip;
    }

  # return the final code and list of states
  ($state->{code}, \@final_states);
  }

sub gather_json_info($)
  {
  my ($path) = @_;

  # returns a structure like this:
  # {
  #   modid => "bricklayers",
  #   json_files => [ list_of_file_names ],		# all JSON files
  #   json_itemtypes => [ list_of_file_names ],		# all assets/itemtypes
  #   json_blocktypes => [ list_of_file_names ],	# all assets/blocktypes
  #   grid_recipes => [ list_of_file_names ],
  #   other_recipes => [ list_of_file_names ],		# non-grid recipes
  #   patches => [ list_of_file_names ],
  #   entities => [ list_of_file_names ],		# traders, ai, etc
  #   codes => { myitem => undef, myotheritem => undef, ... }
  #   variants => { myitem-blue => undef, myitem-green => undef, myotheritem-red => undef, ... }
  # }

  # Step 1: Gather all JSON info from blocktypes, itemtypes
  @RECIPES = ();
  @GRID_RECIPES = ();
  @PATCHES = ();
  @JSONS = ();
  @ITEMS = ();
  @BLOCKS = ();
  @ENTITIES = ();

  # Gather all files and categorize them into definitions and recipes
  find({ wanted => \&gather_json, no_chdir => 1 }, $path);

  my $modinfo = parse_json('modinfo.json');
  my $modid = $modinfo->{modid};

  die("modid $modid does not look valid to me.") unless $modid =~ /^[a-z]+[a-z0-9-]+$/;

  # List of modids that we depend on being there
  my @MODIDS = ( $modid );
  for my $dep (sort keys %{$modinfo->{dependencies}} )
    {
    # skip survival as it falls under game
    next if $dep eq 'survival';
    push @MODIDS, $dep;
    }

  my $valid_codes = {};			# contains all valid "base" codes like "glazedbricks"
  my $valid_variants = {};		# and all their variants like "glazedbricks-milky-red"

  # Parse all JSONs into memory and build lists of codes and variants
  for my $json_file (sort @JSONS)
    {
    my $json = parse_json($json_file);
    die ("$json_file has no valid JSON") unless ref($json);

    my $type = $json_file =~ /blocktypes/ ? 'block' : ($json_file =~ /itemtypes/ ? 'item' : 'unknown');

    my $code = $json->{code};
    die ("$json_file defines no code") unless defined $code;

    die ("$json_file defines no unique code") if exists $valid_codes->{$code};

    # remember this as a valid code and store the type (item/block)
    $valid_codes->{$code} = $type;

    my @parts = ([$code]);	# how many states does this level have?
    my @curr  = (0);		# and which is our current state in each level?

    if (exists $json->{variantgroups})
      {
      my @variant_states = @{ $json->{variantgroups} };

      die("$json_file does not have variantgroups") unless @variant_states > 0;

      for my $state (@variant_states)
        {
        my ($scode, $sstates) = generate_states_from_json($json_file, $state, $json->{skipVariants} // $json->{skipvariants} // []);

        die("$json_file: State $scode from $code defines no list of states.") unless ref($sstates) eq 'ARRAY';

        push @parts, [ @$sstates ];
        push @curr, 0;
        }
      }

    # how many parts do we have?
    my $c = scalar @curr - 1;
    my $built = 0;
    VARIANT:
    while ($curr[0] <= scalar @{$parts[0]})
      {
      # build a variant from all parts
      my $variant = '';
      for my $idx (0 .. $c)
        {
        $variant .= ($variant ? '-' : '') . $parts[$idx]->[$curr[$idx]];
        }
      $built ++;
      print "# build '$variant'\n" if DEBUG > 0;

      $valid_variants->{$variant} = $type;	# remember this variant

      # Always increment the last state by 1. If it overflows, reset it to 0
      # and increment the second-to-last state (like carry in addition).
      # When the first state gets incremented, we are done.
      my $idx = $c;
      INC:
      while ($idx > 0)
	{
	$curr[$idx]++;
	last INC if $curr[$idx] < scalar @{$parts[$idx]};

	# overflow
	$curr[$idx] = 0;
	$idx--;
	}
      # we can stop if the overflow reached the first position
      last VARIANT if ($idx == 0 || $curr[0] > 0);
      }
    }

  print "# Found " . (scalar keys %$valid_codes) . " codes and " . (scalar keys %$valid_variants) . " variants.\n" if DEBUG;

  # A, B, C => "(A|B|C)"
  my $qr_dep = '(' . join( '|', @MODIDS) . ')';
  eval {
    $qr_dep = qr/^$qr_dep:/;
  };

  # return the result
  {
	modid => $modid,
	dependencies => \@MODIDS,
	valid_codes => $qr_dep,
	json_files => \@JSONS,
	other_recipes => \@RECIPES,
	grid_recipes => \@GRID_RECIPES,
	patches => \@PATCHES,
	itemtypes => \@ITEMS,
	blocktypes => \@BLOCKS,
	entities => \@ENTITIES,
	codes => $valid_codes,
	variants => $valid_variants
  };
  }

# End of code

#############################################################################
# Subroutines

sub check_code($ $ $ $ $)
  {
  # Checks that the given code is valid. The code must exist and the code
  # type must match the known type.
  # Returns undef for ok, non-empty string for error.
  my ($info, $file, $name, $code, $type) = @_;

  return "Recipe $name in $file: '$code' has more than one ':'" if $code =~ /:.*:/;

  return "Recipe $name in $file: '$code' not in a valid namespace" if $code !~ $info->{valid_codes};

  return "Recipe $name has unknown output type " . ($type // "null") unless defined $type && $type =~ /^(block|item)/;

  # only check outputs that we can actually verify
  return if $code !~ /^$info->{modid}:/;

  my $start = '';
  $start = $1 if $code =~ /^$info->{modid}:([a-z]+)/;

  return "Recipe $name: '$code' (start: $start) is not valid code" unless exists $info->{codes}->{$start};
  my $code_type = $info->{codes}->{$start};
  return "Recipe $name: Type '$type' for '$code' (start: $start) must be '$code_type' instead" unless $code_type eq $type;

  # check if "foo-bar-*-baz" is also a valid variant
  my $possible_match;
  my $match = $code;
  # "modid:foo-bar-{rock}-baz" = "foo-bar-{rock}-baz"
  $match =~ s/^$info->{modid}://;
  # "foo-bar-{rock}-baz" = "foo-bar-*-baz"
  $match =~ s/\{[a-z0-9]+\}/*/g;
  # "foo-bar-*-baz" = "foo-bar-.*?-baz"
  $match =~ s/\*/\.\*\?/g;
  my $match_type = undef;
  for my $variant (sort keys %{$info->{variants}})
    {
    #print "Comparing $variant vs. $match\n";
    if ($variant eq $match || $variant =~ /^$match$/)
      {
      $possible_match = $variant;
      $match_type = $info->{variants}->{$variant};
      last;
      }
    }
  print "# Found matching variant $possible_match for $code\n" if $possible_match && DEBUG;

  return "$file: '$code' is not a valid variant" unless defined $possible_match;

  # all tests passed
  undef;
  }

# END OF CODE
1;
